var db = require('mysql');

exports.connect = function (conData, callback) {
	var con = db.createConnection({
		host: conData.host,
		user: conData.user,
		password: conData.password,
		database: conData.database
	});
	con.connect(function (err) {
		if (err) callback(err);
		callback(null, con);
	});
};