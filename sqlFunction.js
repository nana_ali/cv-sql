var db = require('./config');

exports.add = function (conData, req, callback) {
	db.connect(conData, function (err, data) {
		if (err) {
			callback(err);
			callback(err);
		}

		{
			var message_details = {

				name: req.body['yourname'],
				phone: req.body['phone'],
				email: req.body['email'],
				websiteUrl: req.body['websiteUrl'],
				message: req.body['message']
			};
			data.query('INSERT INTO contacts SET ?', message_details, function (err, result) {
				callback(err, message_details);

			});
		}
	});
};
exports.getById = function (conData, req, callback) {
	db.connect(conData, function (err, data) {
		if (err) {
			callback(err);
		}
		let id = req.params.id;
		data.query('SELECT * FROM contacts WHERE id = ' + id, function (err, result) {
			let data = JSON.stringify(result);
			callback(err, data);
		});
	});
};

exports.get = function (conData, req, callback) {
	db.connect(conData, function (err, data) {
		if (err) {
			callback(err);
		}
		let id = req.params.id;
		data.query('SELECT * FROM contacts ', function (err, result) {
			let data = JSON.stringify(result);
			callback(err, data);
		});
	});
};

exports.put = function (conData, req, callback) {
	db.connect(conData, function (err, data) {
		var id = req.query.id;
		if (err) {
			callback(err);
			callback(err);
		}
		var message_details = {
			name: req.query.yourname,
			phone: req.query.phone,
			email: req.query.email,
			websiteUrl: req.query.websiteUrl,
			message: req.query.message
		};
		data.query('UPDATE contacts SET ? WHERE id = ' + id, message_details, function (err, result) {

			callback(err, message_details);

		});

	});
};

exports.deleteById = function (conData, req, callback) {

	db.connect(conData, function (err, data) {
		let id = req.params.id;
		if (err) {
			callback(err);
			return;
		}

		data.query('DELETE FROM contacts WHERE id = ' + id, function (err, result) {
			let data = JSON.stringify(result);

			callback(err, data);
		});

	});
};