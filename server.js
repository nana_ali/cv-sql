var express = require('express');
var message = require('./sqlFunction');
var swig = require('swig');
var bodyParser = require('body-parser')
var app = express();
var port = 8080;
const databaseData = {
	host: "localhost",
	user: "root",
	password: "123",
	database: "contactinfo"
};
app.use(bodyParser.urlencoded({
	extended: false
}));
app.get('/', function (req, res) {
	var template = swig.compileFile(__dirname + '/public/cv.html');
	var output = template({});
	res.send(output);
});
app.post('/add', (req, res) => {
	message.add(databaseData, req, function (err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, POST')
		if (err) {
			res.status(400);
			res.end("error:" + err);
			return;
		}

		res.status(201);
		res.end(JSON.stringify({
			message: "message added successfully"
		}));
	});
});

app.get('/api/v1.0/messages/:id', (req, res) => {
	message.getById(databaseData, req, function (err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if (err) {
			res.status(400);
			res.end("error:" + err);
			return;
		}
		res.status(200);
		res.end(data);
	});
});


app.get('/api/v1.0/messages', (req, res) => {
	message.get(databaseData, req, function (err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		if (err) {
			res.status(400);
			res.end("error:" + err);
			return;
		}
		res.status(200);
		res.end(data);
	});
});

app.put('/api/v1.0/update', (req, res) => {

	message.put(databaseData, req, function (err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'PUT')

		if (err) {
			res.status(400);
			res.end("error:" + err);
			return;
		}

		res.status(200);
		res.end("success");
	});
});

app.delete('/api/v1.0/messages/:id', (req, res) => {

	message.deleteById(databaseData, req, function (err, data) {

		if (err) {
			res.status(400);
			res.end("error:" + err);
			return;
		}
		res.status(201);
		res.end(data);
	});
});
app.use(express.static('public'));
app.listen(port);